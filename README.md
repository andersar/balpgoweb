BalpGoWeb
=========

This is an example code for GoLang and app-engine.


Some setup:

Install [Cloud SDK](https://cloud.google.com/appengine/docs/standard/go/setting-up-environment)
as google tutorial. You need a different project and bucket name than mine as they are global.


    gcloud app create
    gcloud config set project balpgoweb
    
The template in hello.html contains two images from a Google Storage:
HiQ_logo_neg_snedstreck.png and background.jpeg, these should (could)
be uploaded to your own storage. 

    gsutil mb gs://balpgoweb.appspot.com/  
    gsutil cp background.jpeg gs://balpgoweb.appspot.com/
    gsutil cp HiQ_logo_neg_snedstreck.png gs://balpgoweb.appspot.com/
    gsutil acl ch -u AllUsers:R background.jpeg
    gsutil acl ch -u AllUsers:R HiQ_logo_neg_snedstreck.png

To deploy the code

    gcloud app deploy
    
View the deployed code

    gcloud app browse